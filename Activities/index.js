function printAlert () {
	let fullName = prompt("Enter Full Name: ");
	let age = prompt("Enter Age: ");
	let location = prompt("Enter Location: ");
	console.log("Hello, " + fullName);
	console.log("You are " +" " + age + " " + "years old.");
	console.log("You live in " + location);
	
}

printAlert();


/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function favoriteBand() {
	console.log("1. Urbandub");
	console.log("2. Updharmadown");
	console.log("3. LANY");
	console.log("4. Spades");
	console.log("5. Postmodern Jukebox");
}

favoriteBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
let topMovies = function moviesRating() {
	console.log("1. Belle");
	console.log("Rotten Tomatoes Rating: 90%");
	console.log("2. The Bad Guy");
	console.log("Rotten Tomatoes Rating: 89%");
	console.log("3. Black Panther: Wakanda Forever");
	console.log("Rotten Tomatoes Rating: 85%");
	console.log("4. Fall");
	console.log("Rotten Tomatoes Rating: 70%");
	console.log("5. Master");
	console.log("Rotten Tomatoes Rating: 37%");
}
topMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printUsers = function printFriends (){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();
